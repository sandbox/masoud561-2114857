<?php

function _admin_panel_variable_set($name, $value) {
  db_merge('admin_panel_variables')->key(array('name' => $name))->fields(array('value' => serialize($value)))->execute();
}

function _admin_panel_variable_get($name) {
  $variable_db = db_select('admin_panel_variables', 'apv')
          ->fields('apv', array('value'))
          ->condition('name', $name)
          ->execute();
  return unserialize($variable_db->fetchColumn());
}

function _admin_panel_variable_del($name) {
  db_delete('admin_panel_variables')
          ->condition('name', $name)
          ->execute();
}

function _admin_panel_get_used_widgets() {
  $db_widgets_obj = db_select('admin_panel_widgets', 'apw')
          ->fields('apw', array('id', 'filename', 'visualname', 'widget_order'))
          ->orderBy('widget_order') //Most recent first.
          ->execute();
  $db_widgets_arr = array();
  foreach ($db_widgets_obj as $widget) {
    $db_widgets_arr[] = array(
        'id' => $widget->id,
        'filename' => $widget->filename,
        'visualname' => $widget->visualname,
        'widget_order' => $widget->widget_order,
    );
  }
  return $db_widgets_arr;
}

if (db_table_exists('admin_panel_widgets') && module_exists('admin_panel')) {
  $active_widgets = _admin_panel_get_used_widgets();
  foreach ($active_widgets as $widget) {
    module_load_include('inc', 'admin_panel', 'widgets/' . preg_replace('/.inc$/', '', $widget['filename']));
  }
}

function _admin_panel_get_widget_themes() {
  $widget_themes = array();
  if (db_table_exists('admin_panel_widgets') && module_exists('admin_panel')) {
    $active_widgets = _admin_panel_get_used_widgets();
    foreach ($active_widgets as $widget) {
      $widget_theme_func_name = '_admin_panel_'.preg_replace('/.inc$/', '', $widget['filename']).'_themes';
      if(function_exists($widget_theme_func_name)) {
        $widget_themes = array_merge($widget_themes, $widget_theme_func_name());
      }
    }
    return $widget_themes;
  }
  else {
    return NULL;
  }
}

function _admin_panel_get_widgets_html() {
  $active_widgets = db_select('admin_panel_widgets', 'apw')
          ->fields('apw', array('id', 'filename', 'visualname', 'widget_order'))
          ->orderBy('widget_order') //Most recent first.
          ->execute();

  $block_contents = '';

  foreach ($active_widgets as $widget) {
    $widget_filename = preg_replace('/.inc$/', '', $widget->filename);
    $widget_func_name = '_admin_panel_' . $widget_filename;
    $widget_html = $widget_func_name();
    $widget_html_wrapper = '<div class="admin_panel_widget_wrapper apww_' . $widget_filename . '">';
    $widget_html_wrapper .= '<div class="admin_panel_widget_header">' . t($widget->visualname) . '</div>';
    $widget_html_wrapper .= '<div class="admin_panel_widget_content apwc_' . $widget_filename . '">';
    $widget_html_wrapper .= $widget_html . '</div></div>';
    $block_contents .= $widget_html_wrapper;
  }
  return $block_contents;
}

function _admin_panel_get_unused_widgets($used_widgets) {
  $widgets_files = scandir(drupal_get_path('module', 'admin_panel') . '/widgets');
  $unused_widgets = array();

  foreach ($widgets_files as $file) {
    if (preg_match('/.inc$/', $file)) {
      $widget_not_used = TRUE;
      foreach ($used_widgets as $used_widget) {
        if ($used_widget['filename'] == $file) {
          $widget_not_used = FALSE;
          break;
        }
      }
      if ($widget_not_used === TRUE) {
        $unused_widgets[] = array(
            'id' => preg_replace('/.inc$/', '', $file),
            'filename' => $file,
            'visualname' => str_replace('_', ' ', preg_replace('/.inc$/', '', $file)),
        );
      }
    }
  }

  return $unused_widgets;
}
