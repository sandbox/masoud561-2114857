<?php

function _admin_panel_administration_shortcuts_get_last_link() {
  $last_link_id = _admin_panel_variable_get('administration_shortcuts_link_last');
  if(isset($last_link_id) && is_numeric($last_link_id) && $last_link_id >= 1) {
    return $last_link_id;
  }
  else {
    return 1;
  }
}

function _admin_panel_administration_shortcuts_set_last_link($last_link_id) {
  _admin_panel_variable_set('administration_shortcuts_link_last', $last_link_id);
}

function _admin_panel_administration_shortcuts_add_link($name, $address) {
  $new_link_id = _admin_panel_administration_shortcuts_get_last_link() + 1;
  _admin_panel_variable_set('administration_shortcuts_link_name_'.$new_link_id, $name);
  _admin_panel_variable_set('administration_shortcuts_link_address_'.$new_link_id, $address);
  _admin_panel_variable_set('administration_shortcuts_link_order_'.$new_link_id, $new_link_id);
  _admin_panel_administration_shortcuts_set_last_link($new_link_id);
}

function _admin_panel_administration_shortcuts_delete_link($link_id) {
  _admin_panel_variable_del('administration_shortcuts_link_name_'.$link_id);
  _admin_panel_variable_del('administration_shortcuts_link_address_'.$link_id);
  _admin_panel_variable_del('administration_shortcuts_link_order_'.$new_link_id);
}

function _admin_panel_administration_shortcuts_get_all_links() {
  $links = array();
  $links_name_query = db_select('admin_panel_variables', 'apv')
          ->fields('apv', array('name', 'value',))
          ->condition('name', db_like('administration_shortcuts_link_name_').'%', 'LIKE')
          ->execute();
  
  $links_address_query = db_select('admin_panel_variables', 'apv')
          ->fields('apv', array('name', 'value',))
          ->condition('name', db_like('administration_shortcuts_link_address_').'%', 'LIKE')
          ->execute();
  
  $links_order_query = db_select('admin_panel_variables', 'apv')
          ->fields('apv', array('name', 'value',))
          ->condition('name', db_like('administration_shortcuts_link_order_').'%', 'LIKE')
          ->execute();
  
  foreach ($links_name_query as $link) {
    $links['id_'.str_replace('administration_shortcuts_link_name_', '', $link->name)] = array(
        'id' => str_replace('administration_shortcuts_link_name_', '', $link->name),
        'name' => unserialize($link->value),
    );
  }
  
  foreach ($links_address_query as $link) {
    if(isset($links['id_'.  str_replace('administration_shortcuts_link_address_', '', $link->name)])) {
      $links['id_'.  str_replace('administration_shortcuts_link_address_', '', $link->name)]['address'] =
              unserialize($link->value);
    }
  }
  
  foreach ($links_order_query as $link) {
    if(isset($links['id_'.  str_replace('administration_shortcuts_link_order_', '', $link->name)])) {
      $links['id_'.  str_replace('administration_shortcuts_link_order_', '', $link->name)]['order'] =
              unserialize($link->value);
    }
  }
  
  if(isset($links) && !empty($links) && !is_null($links)) {
    $links_temp = array();
    foreach ($links as $key=>$link) {
      $links_temp[] = $link;
    }
    $links = $links_temp;
  
    function cmp_by_optionNumber($a, $b) {
      return strcmp($a['order'], $b['order']);
    }
    usort($links, "cmp_by_optionNumber");
  }
  
  return $links;
}

function _admin_panel_administration_shortcuts() {
  $as_html = '';
  $links = _admin_panel_administration_shortcuts_get_all_links();
  if(!empty($links)) {
    foreach ($links as $value) {
      $as_html .= l(t($value['name']), $value['address'])."\n";
    }
  }
  
  return $as_html;
}

function admin_panel_administration_shortcuts_form($form, &$form_state) {
  $form['admin_panel_administration_shortcuts_links'] = array(
      '#type' => 'fieldset',
      '#title' => t('Links settings'),
      '#weight' => 5,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#theme' => 'admin_panel_fieldset_administration_shortcuts_links',
  );
  
  $links = _admin_panel_administration_shortcuts_get_all_links();
  if(!empty($links)) {
    foreach ($links as $link) {
      $form['admin_panel_administration_shortcuts_links']['apasl_' . $link['id']]['apasl_' . $link['id'] . '_name'] = array(
          '#type' => 'markup',
          '#markup' => $link['name'],
      );

      $form['admin_panel_administration_shortcuts_links']['apasl_' . $link['id']]['apasl_' . $link['id'] . '_order'] = array(
          '#type' => 'textfield',
          '#default_value' => $link['order'],
          '#size' => 3,
          '#attributes' => array('class' => array('rank-weight')),
      );

      $form['admin_panel_administration_shortcuts_links']['apasl_' . $link['id']]['apasl_' . $link['id'] . '_id'] = array(
          '#type' => 'hidden',
          '#value' => $link['id'],
      );
    }
  }

  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save Configurations'),
  );

  return $form;
}

function _admin_panel_administration_shortcuts_themes() {
  $themes['admin_panel_fieldset_administration_shortcuts_links'] = array(
      'render element' => 'fieldset',
  );
  
  return $themes;
}

function theme_admin_panel_fieldset_administration_shortcuts_links($variables) {
  $fieldset = $variables['fieldset'];
  drupal_add_tabledrag('admin_panel_administration_shortcuts_links_table', 'order', 'sibling', 'rank-weight');
  
  $links_table_headers = array(
      'link_name' => t('Link Title'),
      'weight' => t('Weight'),
  );
  
  $link_table_rows = array();
  
  foreach (element_children($fieldset) as $key) {
    $link_table_row = array();

    $link_table_row['data'] = array();

    $link_table_row['data'][] = array('data' => $fieldset[$key][$key . '_name'], 'class' => 'admin_panel_administration_shortcuts_links_col1');
    $link_table_row['data'][] = drupal_render($fieldset[$key][$key . '_order']);
    $link_table_row['class'] = array('draggable');

    drupal_render($fieldset[$key][$key . '_id']);
    $link_table_rows[] = $link_table_row;
  }
  
  return theme('table', array(
      'header' => $links_table_headers,
      'rows' => $link_table_rows,
      'attributes' => array('id' => 'admin_panel_administration_shortcuts_links_table'), // needed for table dragging
  ));
}
