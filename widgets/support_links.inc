<?php

function _admin_panel_support_links() {
  $sl_html = '<a href="">support 1</a> ' .
          '<a href="">support 2</a>';
  return $sl_html;
}

function admin_panel_support_links_form($form, &$form_state) {
  $form['admin_panel_style3'] = array(
      '#type' => 'select',
      '#title' => t('Style mode 3:'),
      '#options' => array(
          'style_right' => t('Fix right'),
          'style_left' => t('Fix left'),
          'style_block_custom' => t('inBlock Custom'),
      ),
      '#default_value' => variable_get('admin_panel_style3', 'style_right'),
  );

  return system_settings_form($form);
}