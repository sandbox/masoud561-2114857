var $ = jQuery.noConflict();
$(document).ready(function() {
  $('.admin_panel_widget_wrapper+.admin_panel_widget_wrapper .admin_panel_widget_header').toggle(function(event) {
    $(event.target).parent().children('.admin_panel_widget_content').slideDown(150);
  }, function(event) {
    $(event.target).parent().children('.admin_panel_widget_content').slideUp(150);
  });
  
  $('#block-admin-panel-admin-panel .admin_panel_widget_wrapper:first .admin_panel_widget_header').toggle(function(event) {
    $(event.target).parent().children('.admin_panel_widget_content').slideUp(150);
  }, function(event) {
    $(event.target).parent().children('.admin_panel_widget_content').slideDown(150);
  });
});